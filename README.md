# README #

### Quick summary ###

**Two level cache**

1.	First level of caching is Memory, second level is file.

2.	There are eviction policy such as FIFO, LFU, LRU, Random or NULL 
    that can be set in configuration file.
3.  Also there is configuration parameter that set 
	whether is it used second level of cache or no.


### Build project and run performance tests ###

It's need install ANT before build project.

Some times also need to set property name="ECLIPSE_HOME" in build.xml file
and property "diskStorePath" in Config/Config.xsd file.

Than it used command file "build & run performance tests.bat" or Windows commands:
 
** > cd "project folder" **

** > ant **


### Tests ###

** Tests series #1 **

First of all it take place series of tests with various combination of 
eviction strategy of memory and disk level of cache:

LRU-LFU,

LRU-NULL cache WITHOUT DISK CACHING,

LRU-LRU,

LFU-LFU,

FIFO-LFU,

RANDOM-LFU,

FIFO-LRU.

After this tests you may see a quantity of success and refuse attempts to read caching object.
So you can make decision how combination of eviction policy is more effective.

** Tests series #2 **

The second series of tests are performance tests for various combinations of eviction policy.
Any performance tests consists of 10 batch from 10 till 5000 put/get operations.
After any test you can see average time of put/get operations.

** Tests series #3 **

The third series of tests are CONCURRENT PERFORMANCE tests for various combinations of eviction policy.

Any test run  simultaneously 25 parallel threads. 

Any of this threads run series from 10 till 5000 put/get operations.

After any test you also can see average time of put/get operations.

The main aim of this tests is to be sure that:

- it was not happened any breaks or errors during executing batch of threads,

- threads don't prevented to execute for each other,

- performance isn't change in compare with tests without concurrent threads of series #2. 


** Tests series #4 **

The forth series of tests are Disk Optimization tests for various combinations of eviction policy.

The main aim of this tests is to be sure that optimization procedure not prevent to caching process.


### Analize of results of tests ###

I compared the results of concurrent performance tests of my framework 
with some popular cache framework as 

•	Java Caching System (JCS)

•	ehCache

•	OSCache

•	JBoss Cache


For more details description see "(EN)Compare of performance of Cache-Frameworks.doc".


My caching framework is not distributed. I think this is reason that
my framework has more performance productivity.

But it is strange for me why my framework more productive then local version of ehCache with no distribution opportunity.

For more technical details see "(EN)Cache-Framework.Tecnical Reference.doc".


### Configuration ###

Config file is situated in folder Config and has parameters:

	"name" - name of Cache. The framework get to programmers opportunity use many caches simultaneously. 

	"memoryStoreEvictionPolicy" -  one of FIF0, LFU, LRU, RANDOM 

	"diskStoreEvictionPolicy"   -  one of FIF0, LFU, LRU, RANDOM or NULL if WITHOUT DISK CACHING,

	"diskPersistent"   - set opportunity to restore disk caching data after reboot application. 

	"eternal"    - set opportunity to restore caching data after create new cache with the same name. 

	"diskExpiryThreadIntervalSeconds" - the period of freshness of disk caching data.

	"memoryExpiryThreadIntervalSeconds" - the period of freshness of memory caching data.

	"timeToIdleSeconds"   - maximum downtime of cached object, 

	"timeToLiveSeconds"   - lifetime of memory caching object.

	
Using of following parametrs are obviously:

	"diskSpoolBufferSizeMB", "diskAccessStripes", "maxElementsInMemory", "maxElementsOnDisk",  
	"clearOnFlush", "overflowToDisk", "diskStore", "statistics"