package zrv.willeyCache;

import java.util.Date;
import java.util.Random;
import java.util.concurrent.Callable;

public class Performancer implements Callable<double[]> {
	Cache cache;
		String[] s = {
				"������ ���������� ������� ������ ��� ������������ ������������������ ������/������ � ���",
				"������ ���������� ������� ������ ��� ������������ ������������������ ������/������ � ���",
				"������ ���������� ������� ������ ��� ������������ ������������������ ������/������ � ���"
		};
	
	Performancer(Cache cache) {
		this.cache = cache;
		int capacity = cache.getCapacity();
		for (int i = 0; i < capacity; i++) cache.put(i, s);
	}
	
	public double[] call() throws Exception {
		int capacity = cache.getCapacity();
		Random random = new Random();
		Date begin, end;
		long duration;
		double get, put;
		int index;
		// get block
		begin = new Date();
		for (int i = 0; i < 10; i++) {
			index = random.nextInt(capacity);
//			System.out.print(i + " = " + index + " ");
			cache.get(index);
		}
		end = new Date();
		duration = end.getTime() - begin.getTime();
		get = duration/10.0;
		LOG.info("Put average for MyFramework = " + get + "     ");
		
		// put block
		begin = new Date();
		for (int i = 0; i < 10; i++) cache.put(random.nextInt(capacity * 2), s);
		end = new Date();
		duration = end.getTime() - begin.getTime();
		put = duration/10.0;
		LOG.info("Get average for MyFramework = " + put);
		double[] ret = {get, put};
		return ret;
	}

}
