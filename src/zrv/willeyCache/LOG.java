package zrv.willeyCache;

import org.apache.log4j.Logger;

public class LOG {
	
	private static final Logger logger=Logger.getLogger("Cache ");
	
	public static void info(String message) { 
		logger.info("     " + message);
	}
	
	public static void debug(String message) { 
		logger.debug("-- >  " + message);
	}
	
	public static void error(String message) { 
		logger.error(" !!! " + message);
	}
	public static void fatal(String message) { 
		logger.fatal("--------------------------------------------------------------- \n" 
				+ message 
				+ "\n----------------------------------------------------------------");
	}
	

}
