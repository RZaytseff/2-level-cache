package zrv.willeyCache.disk;

import java.io.*;
import java.util.*;
import zrv.willeyCache.Config;

public class TestDiskCache extends Thread {

	private void test(String fileName) throws Exception {
		Config config = new Config(fileName);
		DiskCache diskCache = new DiskCache(fileName, false);
		int capacity = Integer.parseInt(config.getProperty("maxElementsOnDisk"));
		for (int i = 0; i < capacity; i++) diskCache.put(i, i);
		
		System.out.println("Source diskCache keys:\n       " + diskCache);
		Object q = diskCache.get(5).getClass().getCanonicalName();
		System.out.println("key=" + diskCache.get(5) + ": " + diskCache);
		System.out.println("diskData: " + diskCache.loadDiskData());
		System.out.println("key=" + diskCache.get(0) + ": " + diskCache);
		System.out.println("diskData: " + diskCache.loadDiskData());
		System.out.println("key=" + diskCache.get(1) + ": " + diskCache);
		System.out.println("diskData: " + diskCache.loadDiskData());
		System.out.println("key=" + diskCache.get(9) + ": " + diskCache);
		System.out.println("diskData: " + diskCache.loadDiskData());
		System.out.println("key=" + diskCache.get(5) + ": " + diskCache);
		System.out.println("diskData: " + diskCache.loadDiskData());
		System.out.println("key=" + diskCache.get(4) + ": " + diskCache);
		System.out.println("diskData: " + diskCache.loadDiskData());
		System.out.println("key=" + diskCache.put(2, 222) + ": " + diskCache);
		System.out.println("diskData: " + diskCache.loadDiskData());
		System.out.println("key=" + diskCache.put(11, 11) + ": " + diskCache);
		System.out.println("diskData: " + diskCache.loadDiskData());
		System.out.println("key=" + diskCache.put(12, 12) + ": " + diskCache);
		System.out.println("diskData: " + diskCache.loadDiskData());
		System.out.println("key=" + diskCache.put(1, 111) + ": " + diskCache);
		System.out.println("diskData: " + diskCache.loadDiskData());
		System.out.println("key=" + diskCache.put(5, 555) + ": " + diskCache);
		System.out.println("diskData:" + diskCache.loadDiskData());
		System.out.println("key=" + diskCache.put(15, 15) + ": " + diskCache);
		System.out.println("diskData: " + diskCache.loadDiskData());
		
		
		// backUp optimizing keyStore & diskStore and stop update date Thread 
/*		diskCache.backUp();
		diskCache.restore();
		System.out.println("restored keyIndex: " + diskCache.keyDiskHash);
		System.out.println("restored diskData: " + diskCache.loadDiskData());
*/	}

	public static void main(String[] args) throws Exception {
		System.out.println("diskCache  LRU-LFU TESTING");
		new TestDiskCache().test("Config_RANDOM-FIFO.properties");
	}
}
	
