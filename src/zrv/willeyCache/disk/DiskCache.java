package zrv.willeyCache.disk;

import java.io.*;
import java.util.*;

import zrv.willeyCache.*;
import zrv.willeyCache.memory.*;
import zrv.willeyCache.memory.ConcurrentLinkedHashMap.Entry;

import EDU.oswego.cs.dl.util.concurrent.*;

/**
 * Class implementing a simple disk peristence solution.
 */
public class DiskCache implements Serializable  {

	public Config config;
	
	/**
     * Indicates whether the cache is 'alive', defined as having been
     * initialized, but not yet disposed.
     */
    private transient boolean alive;

    /** the name of the cache */
    private String cacheName;
    
    /** the adapter wich contains the actuall data. */
    private transient DiskCacheAdapter dataFile;
    
    /** the adapter which contains the keys and positions */
    private transient DiskCacheAdapter keyFile;

    /** a map of the keys */
    public transient IMemoryCache keyDiskHash;

    /** a map of the keys */
//    public transient IMemoryCache diskData;

    /** the file describing the root of the diskCache. */
    private transient File rafDir;
        
    /**
     * Each instance of a Disk cache should use this lock to synchronize reads
     * and writes to the underlying storage mechanism.
     */
    private transient final ReadWriteLock storageLock =  new WriterPreferenceReadWriteLock();
    
    /**
     *  currentSize of diskCache
     */
    private int currentSize;

	public transient DiskCacheOptimizer optimzerDiskCache;
	
	
    /**
     * Creates a new Default DiskCache object.
     * @param attributes the attributes for this disk cache.
     * @throws Exception 
     */
	public DiskCache()  {
		config = new Config();
		diskCacheInit(config.cacheConfigDefault, true);
	}
	
    
    /**
     * Creates a new DiskCache object  from config properties file.
     * @param attributes the attributes for this disk cache.
     * @throws Exception 
     */
	public DiskCache(String congigFile)  {
		config = new Config();
		diskCacheInit(config.cacheConfig, true);
  }
    
	public DiskCache(String congigFile, boolean optimizing)  {
		if (congigFile == null) congigFile = "Config.properties";
		config = new Config(congigFile);
		diskCacheInit(config.cacheConfig, optimizing);
  }
    
    /** 
	* Creates a newest DiskCache object.
     * @param attributes the attributes for this diskCache.
     * @throws Exception 
     */
	public DiskCache(Properties cacheConfig) throws Exception {
        this.cacheName = cacheConfig.getProperty("cacheName","WilleyCache");
        diskCacheInit(cacheConfig, true);
	}
	   
    private void diskCacheInit(Properties cacheConfig, boolean optimizing) {
    	if (config == null) config = new Config();
    	config.cacheConfig = config.addDefaultProperties(cacheConfig);
        rafDir = new File(config.getProperty("diskStorePath"));
        rafDir.mkdirs();
        dataFile = new DiskCacheAdapter(new File(rafDir, config.getProperty("diskStoreName") + ".data"));
        keyFile  = new DiskCacheAdapter(new File(rafDir, config.getProperty("diskStoreName") + ".key"));
        keyDiskHash = DiskCacheFactory.newInstance(config.cacheConfig);

        if (keyFile.length() > 0) {
        	try {
        		loadKeysFromFile();
        	} catch (Exception e) {
        	}
            if (keyDiskHash.size() == 0) {
                dataFile.reset();
            }
        } else {
            keyDiskHash = DiskCacheFactory.newInstance(cacheConfig);
            if (dataFile.length() > 0) {
			//  temporally don't make reset during debug
			// TODO return back a reset after debug
            // dataFile.reset();
            }
        }
        alive = true;
        
        /** run a sheduling optimizer that time to time 
         * provide indexed keyFile in correspondence to cache policy
         */
        optimzerDiskCache = new DiskCacheOptimizer(cacheConfig, keyDiskHash
        								, dataFile
        								, keyFile
        								, Integer.parseInt
        									(config.getProperty("diskBackUpThreadIntervalSeconds")));
        	
        if (optimizing) new Thread(optimzerDiskCache).start();
    }
    
    /**
     * gets an object from the disk cache
     *
     * @param key the key for the object
     *
     * @return an object from the disk cache
     *
     * @throws DiskCacheException if exceptions occur.
     */
    private Serializable doGet(Serializable key) throws Exception {
        try {
            storageLock.readLock().acquire();
            if (!alive) {
                return null;
            }
            return readElement(key);
        } catch (InterruptedException e) {
            throw new DiskCacheException("The read was interrupted.");
        } finally {
            storageLock.readLock().release();
        }
    }

    /**
     * Update the disk cache. 
     * @param the object to write.
     * @throws DiskCacheException if exceptions occur.
     */
    private void doUpdate(Object key, byte[] data) {
        try {
            DiskKeyEntry dKE = new DiskKeyEntry();
//            LOG.info("dataFile.length() " + dataFile.length()); 
            if (dataFile.length() < 0) dKE.init(0, data);
            else dKE.init(dataFile.length(), data);
            storageLock.writeLock().acquire();
            if (!alive) {
                return;
            }
            DiskKeyEntry old =
                (DiskKeyEntry) keyDiskHash.put(key, dKE);
            if ((old != null) && (dKE.length <= old.length)) {
            	dKE.pos = old.pos;
            }
            dataFile.write(data, dKE.pos);
        } catch (InterruptedException e) {
            LOG.error("IMPOSSIBLE TO WRITE DISK CACHE: probably it is use by another process"
            		+ e.getMessage() 
            		+ e.getStackTrace());
        } finally {
            storageLock.writeLock().release();
        }
    }

    public final Serializable get(final Serializable key)  {
    	try {
    		return getObject(key);
    	} catch (Exception e) {
    		return null;
    	}
    	
//    	return DiskCacheAdapter.deSerialize((byte[]) getObject(key));
    }
    
    /**
     * gets an object from the diskCache
     * @param key the key for the object
     * @return an object from the diskCache
     * @throws Exception 
     */
    public final Serializable getObject(final Serializable key)  throws Exception {
        if (!alive) {
            return null;
        }
        return doGet(key);
    }

    /**
     * loads keys from an existing diskCache
     * @throws DiskCacheException
     *
     * @throws DiskCacheException if exceptions occur.
     * @throws DiskCacheException 
     */
    public void loadKeysFromFile() throws Exception  {
        try {
            storageLock.readLock().acquire();
            keyDiskHash = (IMemoryCache)keyFile.readObject(0);
            if (keyDiskHash == null) {
                keyDiskHash = DiskCacheFactory.newInstance(config.cacheConfig);
            }
        } catch (InterruptedException e) {
            ;
        } finally {
            storageLock.readLock().release();
        }
    }

    public Object loadDiskData() throws Exception  {
    	return loadDiskData(config.cacheConfig);
    }

    public Object loadDiskData(Properties cacheConfig) throws Exception  {
        try {
        	IMemoryCache diskData = DiskCacheFactory.newInstance(config.cacheConfig, false);
            storageLock.readLock().acquire();
            long index =0;
            long len = dataFile.length();
            while (index < dataFile.length()) {
            	Object dataEntry = dataFile.readObject(index);
            	diskData.put(index, dataEntry);
            	len = (dataFile.serialize((Serializable)dataEntry)).length;
            	index+=(dataFile.serialize((Serializable)dataEntry)).length + 4; // 4 byte length before data
            }
            return diskData;
        } catch (InterruptedException e) {
            return null;
        } finally {
            storageLock.readLock().release();
        }
    }

    public void restore() {
    	try {
			loadKeysFromFile();
		} catch (Exception e) {
			LOG.error("FAULT restore keyIndex of diskCache" + e.getMessage());
			e.printStackTrace();
		}
    }
    
    /**
     * closes the diskCache, and optimizes the disk files.
     * @throws Exception 
     */
    public void backUp() throws Exception {
        try {
            storageLock.writeLock().acquire();
            if (!alive) {
                return;
            }
        	optimzerDiskCache.go = false;
            if (!optimzerDiskCache.process) optimizeCache();
            dataFile.close();
            dataFile = null;
            keyFile.close();
            keyFile = null;
        } catch (DiskCacheException e) {
            //duh, so what. its closed down and useless anyway...
            //the user will get the error when he tries to open it the next time.
        } catch (InterruptedException e) {
            ;
        } finally {
            alive = false;
            storageLock.writeLock().release();
            
        }
    }

    /**
     * optimize the diskCache according to Cache Strategy.
     * @throws Exception 
     * 
     * @TODO make this operation asynchronous to speed up flushing.
     */

    private void optimizeCache() throws Exception {
    	String diskStoreName = config.getProperty("diskStoreName");
    	IMemoryCache keyDiskHashTemp = 
    					 DiskCacheFactory.newInstance(config.cacheConfig);
        DiskCacheAdapter dataFileTemp =
            new DiskCacheAdapter(new File(rafDir, diskStoreName + "Temp.data"));
		Entry e = keyDiskHash.getHead();
		while (e != null) {
			Serializable key = (Serializable) e.getKey();
			Serializable value = readElement(key);
			Serializable de = dataFileTemp.appendObject(value);
			keyDiskHashTemp.put(key, de);
			e = e.getSucc();
		}
        dataFileTemp.close();
        dataFile.close();
        File oldData = new File(rafDir, diskStoreName + ".data");
        if (oldData.exists()) {
            oldData.delete();
        }
        File newData     = new File(rafDir, diskStoreName + "Temp.data");
        File newFileName = new File(rafDir, diskStoreName +     ".data");
        if (newData.exists()) {
            newData.renameTo(newFileName);
        }
        keyFile.reset();
        if (keyDiskHash.size() > 0) {
            keyFile.writeObject((Serializable) keyDiskHashTemp, 0);
        }
        
        synchronized(keyDiskHash) {
        	keyDiskHash = keyDiskHashTemp;
        }
        
    }

    /**
     * reads an element from the diskcache
     * @param key - the key for the diskobject
     * @return an element from the diskcache
     * @throws DiskCacheException if exceptions occur.
     * @throws DiskCacheException 
     */
 	Serializable readElement(final Serializable key) throws Exception {
        DiskKeyEntry dke = (DiskKeyEntry) keyDiskHash.get(key);
        if (dke != null) {
            Serializable readObject = dataFile.readObject(dke.pos);
			return  readObject;
        }
        LOG.info("The object " + key
            + " was not found in the diskCache.");
        return null;
    }

 
    public final Serializable put(final Object key, final Object cacheElement) 
    											throws DiskCacheException {
    	return update(key, cacheElement);
    }
    
    /**
     * Adds the provided element to the cache. 
     * @param cacheElement the element to add.
     * @throws IOException 
     * @throws FileNotFoundException 
     */
    public final boolean update(final Object key, final Object cacheElement) 
    								throws DiskCacheException {
        byte[] data = DiskCacheAdapter.serialize((Serializable) cacheElement);
        int newSize = currentSize+data.length;
		int maxSize = Integer.parseInt(config.getProperty("maxElementsOnDisk"))*1024*1024;
		if(newSize > maxSize) {
        	return false;
        }
		doUpdate(key, data);
        currentSize=newSize;
        return true;
    }
    
    public final void reset() {
    	keyFile.reset();
    	dataFile.reset();
    }

    /**
     * Will remove all objects in this diskCache. Just a quick and dirty
     * implementation to make things work.
     *
     * @throws DiskCacheException if removed was not successfull.
     *
     * @todo make this operation asynchronous to speed up flushing.
     */
	public void removeAll() throws DiskCacheException {
        try {
            storageLock.writeLock().acquire();
        } catch (InterruptedException e) {
            ;
        }
        try {
            if(dataFile!=null)dataFile.reset();
            if(keyFile!=null)keyFile.reset();
            currentSize=0;
        } finally {
            storageLock.writeLock().release();
        }
    }

    public String toString() {
    	StringBuffer s = new StringBuffer();
    	Entry e = keyDiskHash.getHead();
    	while (e != null) {
    		s.append(e.getKey()).append("   ");
    		e = e.getSucc();
    	}
    	return s.toString();
    }
}
