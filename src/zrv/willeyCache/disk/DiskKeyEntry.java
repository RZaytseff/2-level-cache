package zrv.willeyCache.disk;

import java.io.Serializable;

	/**
	 * Disk objects are located by descriptor entries. These are saved on shutdown
	 * and loaded into memory on startup.
	 */
public class DiskKeyEntry implements Serializable {
	    /** the position of diskCacheFiole where the object starts */
	    long pos;

	    /** the length of the object */
	    int length;


		DiskKeyEntry() { }

	    /**
	     * initializes this descriptor with the position to start at, 
	     * and a byte[] with the actual data which is to be written.
	     *
	     * @param pos the starting position
	     * @param data the data to write.
	     */
	    void init(final long pos, final byte[] data) {
	        this.pos = pos;
	        this.length = data.length;
	    }
	    
	    void init(final long pos, final int length) {
	        this.pos = pos;
	        this.length = length;
	    }

	    public synchronized int getLength() {
			return length;
		}

		public synchronized void setLength(int length) {
			this.length = length;
		}
		
	    public synchronized long getPos() {
			return pos;
		}

		public synchronized void setPos(long pos) {
			this.pos = pos;
		}

		public String toString() {
	    	return " pos = " + pos + ", length = " + length;  	
	    }
}
