package zrv.willeyCache.disk;

import java.util.Properties;

import zrv.willeyCache.ICache;
import zrv.willeyCache.memory.*;

public class DiskCacheFactory {
	
	static IMemoryCache diskCache = null;
	
	public static IMemoryCache newInstance() {
		return new LFUMemoryCache(1000000);
	}
	
	public static IMemoryCache newInstance(Properties cacheConfig) {
			return newInstance(cacheConfig, false);
	}

	public static IMemoryCache newInstance(Properties cacheConfig, boolean continuum) {
		if (cacheConfig == null) return newInstance();
		int capacity = Integer.parseInt(cacheConfig.getProperty("maxElementsOnDisk", "1000000"));
		String policy = cacheConfig.getProperty("diskStoreEvictionPolicy", "LFU").trim().toUpperCase();

		if (policy.equals("LRU"))    return new    LRUMemoryCache(capacity);
		if (policy.equals("LFU"))    return new    LFUMemoryCache(capacity);
		if (policy.equals("FIFO"))   return new   FIFOMemoryCache(capacity);
		if (policy.equals("RANDOM")) return new RandomMemoryCache(capacity);
		if (!continuum) return new LFUMemoryCache(capacity);
		return (IMemoryCache) new ConcurrentLinkedHashMap();		
	}
}
