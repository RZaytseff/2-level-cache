package zrv.willeyCache.disk;

	import java.io.IOException;
	import java.io.Serializable;

	/**
	 * A CacheObject which is used to send and retrieve
	 * data to and from the diskCache.
	 */
	public class DiskDataEntry implements Serializable {
	    private int length;
	    private Object data;

	private void writeObject(java.io.ObjectOutputStream out) throws IOException {
        out.writeObject(length);
        out.writeObject(data);
	}

    private void readObject(java.io.ObjectInputStream in) throws IOException, ClassNotFoundException {
        this.data=in.readObject();
    }

	public synchronized int getLength() {
		return length;
	}

	public synchronized void setLength(int length) {
		this.length = length;
	}

	public synchronized Object getData() {
		return data;
	}

	public synchronized void setData(Object data) {
		this.data = data;
	}
}
