package zrv.willeyCache.disk;

import java.io.File;
import java.io.Serializable;
import java.util.Iterator;
import java.util.Properties;

import EDU.oswego.cs.dl.util.concurrent.ReadWriteLock;
import EDU.oswego.cs.dl.util.concurrent.WriterPreferenceReadWriteLock;

import zrv.willeyCache.LOG;
import zrv.willeyCache.memory.IMemoryCache;
import zrv.willeyCache.memory.ConcurrentLinkedHashMap.Entry;

public class DiskCacheOptimizer implements Runnable {
	private Properties cacheConfig;
	private IMemoryCache keyDiskHash;
	private DiskCacheAdapter dataFile;
	private DiskCacheAdapter keyFile;
	private int initialDelay;   // delay for first run
	private int timeInterval;   // process period run
	
	public static volatile boolean go;
	public static volatile boolean process;
	private static boolean[] monitorTread = new boolean[2] ;

	public DiskCacheOptimizer(Properties cacheConfig, IMemoryCache keyDiskHash
													, DiskCacheAdapter dataFile
													, DiskCacheAdapter keyFile
													, int timeInterval) {
		this.cacheConfig = cacheConfig;  
		this.keyDiskHash = keyDiskHash;
		this.dataFile = dataFile;
		this.keyFile = keyFile;
		this.timeInterval = timeInterval;
		
		this.go = true;
		this.process = false;
		monitorTread[0] = go;
		monitorTread[1] = process;
	}
	
 	Serializable readElement(final Serializable key) throws Exception {
        DiskKeyEntry dke = (DiskKeyEntry) keyDiskHash.get(key);
        if (dke != null) {
            Serializable readObject = dataFile.readObject(dke.pos);
			return  readObject;
        }
        throw new DiskCacheException("The object " + key
            + " was not found in the diskCache.");
    }
 	
	public void run() {
		while (go) {
			try {
				synchronized (monitorTread) { // that's need for correct threads monitoring
//					LOG.info("\n  Waiting for begin optimizing data on disk = " + go 
//							+ "  ;  process is running = " + process);
					monitorTread.wait(timeInterval * 1000);
				}
				if (go && !process) {
					LOG.info("\n  Begin optimazing data on disk = " + go 
							+ "  ;  process is running = " + process);
					
					process = true;
					LOG.info("\n  Update data on disk = " + go 
							+ "  ;  process is running = " + process);
					
			        String diskStoreName = cacheConfig.getProperty("diskStoreName");
			        String diskStorePath = cacheConfig.getProperty("diskStorePath");
			        File rafDir = new File(diskStorePath);
			        rafDir.mkdirs();
			    	IMemoryCache keyDiskHashTemp = 
						(IMemoryCache) DiskCacheFactory.newInstance(cacheConfig);
			    	
			    	
					DiskCacheAdapter dataFileTemp =
							new DiskCacheAdapter(new File(rafDir, diskStoreName + "Temp.data"));
					
					Entry e = keyDiskHash.getHead();
					while (e != null) {
						Serializable key = (Serializable) e.getKey();
						Serializable value = readElement(key);
						Serializable de = dataFileTemp.appendObject(value);
						keyDiskHashTemp.put(key, de);
						e = e.getSucc();
					}
					dataFileTemp.close();
//					TODO close after debug
//					dataFile.close();
					File oldData = new File(rafDir, diskStoreName + ".data");
					if (oldData.exists()) 
						// oldData.delete();
						oldData.renameTo(new File(rafDir, diskStoreName + "Dump.data"));
					File newData     = new File(rafDir, diskStoreName + "Temp.data");
					File newFileName = new File(rafDir, diskStoreName +     ".data");
					if (newData.exists()) newData.renameTo(newFileName);
					
					
//	TODO reset after debug
//					keyFile.reset();
					if (keyDiskHash.size() > 0) 
						keyFile.writeObject((Serializable) keyDiskHashTemp, 0);
//					TODO reset after debug
//						keyFile.close();
					
					synchronized(keyDiskHash) {
						keyDiskHash = keyDiskHashTemp;
					}
				}

				} catch (Exception e) {
//					LOG.error("FAULT OPTIMIZE DISK CACHE!  cause: \n" + e.getMessage());
//					e.printStackTrace();
				} finally {
					synchronized (monitorTread) {
//						TODO reset after debug
//						dataFile.reset();
						process = false;
						}
						LOG.info("\n End optimazing data on disk.   REPEAT OPTIMIZE GO = " + go 
								+ "  ;  process is running = " + process);
// TODO  exclude after debug
						try {
							LOG.info("Data CACHE ON DISK: \n" + loadDiskData(cacheConfig));
						} catch (Exception e) {
							LOG.error("DON'T READ OPTIMIZED DISK CACHE DATA");
							e.printStackTrace();
						}
				}
			
		}
	}

	   public Object loadDiskData(Properties cacheConfig) throws Exception  {
	        ReadWriteLock storageLock =  new WriterPreferenceReadWriteLock();
			try {
	        	IMemoryCache diskData = DiskCacheFactory.newInstance(cacheConfig, false);
//	        	IMemoryCache keyIndex = DiskCacheFactory.newInstance(cacheConfig, false);
	            storageLock.readLock().acquire();
	            long indexData =0;
	            long indexKey =0;
	            long lengthData = dataFile.length();
	            long lengthKey = keyFile.length();
	            while (indexData < dataFile.length()) {
	            	Object dataEntry = dataFile.readObject(indexData);
//	            	Object keyEntry = keyFile.readObject(indexKey);
//	            	diskData.put(((Entry)keyEntry).getKey(), dataEntry);
	            	diskData.put(indexData, dataEntry);
	            	indexData+=(dataFile.serialize((Serializable)dataEntry)).length + 4; // 4 byte length before data
//	            	lengthKey+=(keyFile.serialize((Serializable)keyEntry)).length + 4;
	            }
	            return diskData;
	        } catch (InterruptedException e) {
	            return null;
	        } finally {
	            storageLock.readLock().release();
	        }
	    }

}
