package zrv.willeyCache.disk;

import java.io.*;

import zrv.willeyCache.LOG;

/**
 * Provides thread safe access to the underlying random access file.
 */
public class DiskCacheAdapter {
    /** the path to the files */
    private String filepath;

    /** an accessor to the file */
    private RandomAccessFile raf;
   
    /**
     * Creates a new CacheFileAdapter object.
     * @param file the file to adapt to
     * @throws DiskCacheException if any exceptioins occur.
     */
    public DiskCacheAdapter(final File file) {
        this.filepath = file.getAbsolutePath();
        try {
            raf = new RandomAccessFile(filepath, "rw");
        } catch (FileNotFoundException e) {
            throw new IllegalStateException(
                "The disk cache could not be initialized.");
        }
    }

    /**
     * returns a String representation of this object.
     * @return a String representation of this object.
     */
    public String toString() {
        return getClass().getName() + ", file:" + filepath;
    }

    /**
     * reads from the diskCache, and return the raw bytes
     * contained at that pos. Its up to the client to deSerialize.
     * @param pos the position to start at.
     * @return a byte[] containing the raw bytes for the object.
     */
    public byte[] read(long pos) {
        byte[] data = null;
        boolean corrupted = false;
        try {
            synchronized (this) {
                raf.seek(pos);
                int datalen = raf.readInt();
                if (datalen > raf.length()) {
                    corrupted = true;
                } else {
                    raf.readFully(data = new byte[datalen]);
                }
            }
            if (corrupted) {
                throw new IllegalStateException("The Cache file is corrupted.");
            }
            return data;
        } catch (IOException e) {
            throw new IllegalStateException(e.getMessage());
        } 
    }
    
    /**
     * reads an object starting at the position,.
     * @param pos the position to start at
     * @return a Serializable object read from the diskCache.
     * @throws DiskCacheException if any exceptions occur.
     */
	public Serializable readObject(long pos) throws DiskCacheException {
        byte[] data = read(pos);
        return deSerialize(data);
    }

	public synchronized void append(byte[] data) throws DiskCacheException {
        try {
            write(data, raf.length());
        } catch (IOException e) {
            throw new DiskCacheException(e);
        }
    }

    public synchronized void write(byte[] data, long pos) {
        try {
            raf.seek(pos);
            raf.writeInt(data.length);
            raf.write(data);
        } catch (IOException e) {
        	LOG.error(e.getMessage());
//            throw new IllegalStateException(e.getMessage());
        }
    }

    /**
     * writes an object down to the diskCache
     *
     * @param obj the object to write
     * @param pos the position to start at.
     *
     * @throws DiskCacheException if any exception occur.
     */
    public void writeObject(final Serializable obj, final long pos) {
        write(serialize((Serializable)obj), pos);
    }

    /**
     * adds an object to the diskCache.
     * @param obj the object to add.
     * @return a disk element descriptor used to locate the object
     * @throws DiskCacheException if any exceptions occur.
     */
    public DiskKeyEntry appendObject(Object obj) {
        long pos = -1;
        DiskKeyEntry dKE = new DiskKeyEntry();
        byte[] data = serialize((Serializable) obj);
        synchronized (this) {
            pos = length();
            dKE.init(pos, data);
            write(data, pos);
        }
        return dKE;
    }

    public synchronized long length() {
        try {
            return raf.length();
        } catch (IOException e) {
        	
 //           throw new IllegalStateException(e.getMessage());
        }
        return -1;
    }

    public synchronized void close() {
        try {
            raf.close();
        } catch (IOException e) {
            throw new IllegalStateException(e.getMessage());
        }
    }

    public synchronized void reset() {
        close();
        File f = new File(filepath);
        //boolean deleted = f.delete();
        int retries=10;

        while (!f.delete()) {
            retries--;
            if(retries<=0) {
            	return;
//            	throw new IllegalStateException("Failed to delete " + f.getName());
            }
            // TODO busy wait.
            try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				//interrupted.
			}
        }
        try {
            raf = new RandomAccessFile(filepath, "rw");
        } catch (FileNotFoundException e) {
            throw new IllegalStateException(e.getMessage());
        }
    }

    public static byte[] serialize(Serializable obj) {
        ObjectOutputStream oos = null;
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            oos = new ObjectOutputStream(baos);
            oos.writeObject(obj);
            return baos.toByteArray();
        } catch (NotSerializableException e) {
            throw new IllegalStateException(e.getMessage());
        } catch (IOException e) {
            throw new IllegalStateException(e.getMessage());
        } finally {
            if (oos != null) {
                try {
                    oos.close();
                } catch (IOException e) {
                    throw new IllegalStateException(
                        "Failed to close stream to DiskCache, "
                        + "cache may be corrupted. Reason: " + e.getMessage());
                }
            }
        }
    }

    public static Serializable deSerialize(byte[] data) throws DiskCacheException {
        ObjectInputStream ois = null;
        try {
            ois=new ObjectInputStream(new BufferedInputStream(
            							new ByteArrayInputStream(data)));
            /** @TODO log */
            return (Serializable) ois.readObject();
 //           DiskDataEntry obj = (DiskDataEntry) ois.readObject();
 //           if (obj.getLength() != ((Byte[])obj.getData()).length)
//            	throw new IllegalStateException("An exception occured when reading from the disk cache." +
//                		" Reason: DATA CORRUPTED !!!!!!!!!!!!!!!!!!!!!" );
//            return (Serializable) obj.getData();
        } catch (IOException e) {
            throw new IllegalStateException("An exception occured when reading from the disk cache." +
            		" Reason: " + e.getMessage());
        } catch (ClassNotFoundException e) {
            throw new IllegalStateException("A class could not be found when deSerializing." +
            		" Reason: " + e.getMessage());
        } finally {
	        if (ois != null) {
	            try {
	                ois.close();
	            } catch (IOException e) {
	                throw new IllegalStateException(
	                    "Failed to close stream to DiskCache, "
	                    + "cache may be corrupted. Reason: " + e.getMessage());
	            }
	        }
        }
    }
}
