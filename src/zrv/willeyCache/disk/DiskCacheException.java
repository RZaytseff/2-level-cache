package zrv.willeyCache.disk;

import zrv.willeyCache.CacheException;

/**
 * An exception was caught or generated within the disk cache management
 * system.
 */
public class DiskCacheException extends CacheException {

	private static final long serialVersionUID = -202744673824585528L;

	/**
     * Constructs an <code>DiskCacheException</code> with the specified detail
     * Exception and additional message.
     *
     * @param message the detail message.
     * @param cause the detail exception.
     */
    public DiskCacheException(final String message, final Exception cause) {
        super(message, cause);
    }

    /**
     * Constructs an <code>DiskCacheException</code> with the specified detail
     * Exception.
     *
     * @param cause the detail exception.
     */
    public DiskCacheException(final Exception cause) {
        super(cause);
    }

    /**
     * Constructs an <code>DiskCacheException</code> with the specified detail
     * message.
     *
     * @param msg the detail message.
     */
    public DiskCacheException(final String msg) {
        super(msg);
    }
}
