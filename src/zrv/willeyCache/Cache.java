package zrv.willeyCache;

import java.io.*;
import java.util.Properties;

import zrv.willeyCache.disk.*;
import zrv.willeyCache.memory.*;
import zrv.willeyCache.memory.ConcurrentLinkedHashMap.Entry;

public class Cache implements ICache {
	public  String cacheName;
	public Config config;
	public Properties cacheConfig;
	private MemoryCache memoryCache;;
	private DiskCache diskCache;
	private boolean overflowToDisk;
	
	public Cache() {
		this.cacheName = "WilleyCache";
		config = new Config();
		init(config.cacheConfig);
	}

	public Cache(String fileName) {
		init(fileName);
	}
	
	private void init(String fileName) {
		init(fileName, true); 
	}
	
	public Cache(String fileName, boolean optimizing) {
		init(fileName, optimizing);
	}
	
	public void init(String fileName, boolean optimizing) {
		try {
			memoryCache = new MemoryCache(fileName);
			diskCache = new DiskCache(fileName, optimizing);
			config = memoryCache.config;
			cacheConfig = config.cacheConfig;
			cacheName = cacheConfig.getProperty("cacheName"); 
			overflowToDisk = Boolean.parseBoolean(config.cacheConfig.getProperty("overflowToDisk", "true"));
		} catch (Exception e) {
			LOG.fatal("Don't create CACHE!!! cause: \n" + e.getMessage());
			e.printStackTrace();
		}
		LOG.info("INITIALIZE CACHE " + cacheName);
	}
	
	
	public Cache(String cacheName, String configFile) {
		init(cacheName, configFile);
	}
		
	private void init(String cacheName, String configFile) {		
		this.cacheName = cacheName;		
		memoryCache = new MemoryCache(configFile);
		overflowToDisk = Boolean.parseBoolean(cacheConfig.getProperty("overflowToDisk", "true"));
		if (overflowToDisk)
			try {
				diskCache = new DiskCache(configFile);
			} catch (Exception e) {
				LOG.fatal("Don't create DISK CACHE!!! cause: \n" + e.getMessage());
				e.printStackTrace();
			}
	}

	public Cache(Properties cacheConfig) {
		init(cacheConfig);
	}

	private void init(Properties cacheConfig) {
		this.cacheConfig = cacheConfig;
		this.overflowToDisk = Boolean.parseBoolean(cacheConfig.getProperty("overflowToDisk"));
		memoryCache = memoryCache = new MemoryCache(cacheConfig);
		try {
			diskCache = new DiskCache(cacheConfig);
		} catch (Exception e) {
			LOG.fatal("Don't create DISK CACHE!!! cause: \n" + e.getMessage());
			e.printStackTrace();
		}
	}

	
	public final synchronized Object put(Object key, Object value) {
		// TODO log after testing
		// LOG.info("������ ������� " + value + " � ������ " + key);
		if (key == null) return null;
		Entry e =  (Entry) memoryCache.putEntry(key, value);
		if (e != null && overflowToDisk)   
			try {
				return diskCache.put(e.getKey(), e.getValue());
			} catch (DiskCacheException dce) {
				LOG.info("MISS: ������ ������� � ������ " + key 
						+ " �� �������:\n" + dce.getMessage());
				dce.printStackTrace();
				return null;
			}
		return null;
	}

	public final synchronized Object get(Object key) {
		// TODO log after testing
		//LOG.info("����� ������� � ������ " + key);
		if (key == null) return null;
		Object o = memoryCache.get(key);
		if (o != null) return o;
		else if(overflowToDisk)
			try {
				return diskCache.get((Serializable)key);
			} catch (Exception exception) {
				LOG.info("MISS: ������ � ������ " + key + " �� ������:\n" + exception.getMessage());
				exception.printStackTrace();
				return null;
			}
		return o;
	}
	/**
	 * backUp memory data and indexes of diskCache to disk file 
	 * independently of config parameters
	 */
	public final void backUp() {
		memoryCache.backUp();
		try {
			memoryCache.backUp();
			diskCache.backUp();
		} catch (Exception e) {
			LOG.fatal("FAULT BACKUP CACHE! cause: \n" + e.getMessage());
			e.printStackTrace();
		}
	}

	/**
	 * RESTORE or NO memory data and indexes of diskCache from disk file 
	 * dependence of config parameters
	 */
	public final void restore() {
		try {
			// test for need of restore memory Cache from disk BackUp
			boolean eternal = Boolean.parseBoolean(cacheConfig.getProperty("eternal")); 
			if (eternal) memoryCache.restore();
			// test for need of restore disk Cache or reset
			boolean diskPersistent = Boolean.parseBoolean(cacheConfig.getProperty("diskPersistent")); 
			if (diskPersistent) diskCache.restore();
			else diskCache.reset();
		} catch (Exception e) {
			LOG.fatal("FAULT RESTORE CACHE! cause: \n" + e.getMessage());
			e.printStackTrace();
		}
	}

	public String toString() {
 		return "memoryCache: " + memoryCache + "\n" +
			   "  diskCache: " + diskCache;
	}

	public int getMemoryCapacity() {
		return memoryCache.getCapacity();
	}
	
	public int memorySize() {
		return memoryCache.size();	
	}

	@Override
	public int getCapacity() {
		return getMemoryCapacity();
	}

	@Override
	public int size() {
		return memorySize() ;
	}
	
	public String getProperty(String property) {
		return cacheConfig.getProperty(property);
	}

	@Override
	public void clear() {
		memoryCache.clear();
	}

	@Override
	public Object remove(Object key) {
		return memoryCache.remove(key);
	}

	public synchronized Properties getCacheConfig() {
		return cacheConfig;
	}
	
	public boolean diskOptimizerGO(boolean go) {
		boolean went = diskCache.optimzerDiskCache.go;
		diskCache.optimzerDiskCache.go = go;
		return went;
	}

}
