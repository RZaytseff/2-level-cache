package zrv.willeyCache.memory;

import java.io.*;
import java.util.*;
import java.util.concurrent.*;

import org.apache.log4j.Logger;

import zrv.willeyCache.Config;
import zrv.willeyCache.ICache;
import zrv.willeyCache.LOG;
import zrv.willeyCache.memory.MemoryCacheFactory;
import zrv.willeyCache.memory.ConcurrentLinkedHashMap.Entry;

public class MemoryCache implements ICache, Serializable {

    /** the filename of config properties*/
    private String configFileName;

    /** the filename of default config properties*/
    private String configFileNameDefault;

	public String cacheName;
	public Config config;
	
	private IMemoryCache cache;
	
	public MemoryCache() {
		config = new Config();
		cacheName = config.cacheName;
		cache = MemoryCacheFactory.newInstance(config.cacheConfig);
		}

	public MemoryCache(int capacity) {
		config = new Config();
		cacheName = config.cacheName;
		config.cacheConfig.setProperty("maxElementsInMemory", "" + capacity);
		cache = MemoryCacheFactory.newInstance(config.cacheConfig);		
	}
	
	public MemoryCache(int capacity, String policy) {
		config = new Config();
		cacheName = config.cacheName;
		config.cacheConfig.setProperty("maxElementsInMemory", "" + capacity);
		config.cacheConfig.setProperty("memoryStoreEvictionPolicy", "LRU");
		cache = MemoryCacheFactory.newInstance(config.cacheConfig);		
	}
	
	public MemoryCache(String configFile) {
		if (configFile == null) configFileName = "Config.properties";
		else configFileName = configFile;
		config = new Config(configFile);
		cache = MemoryCacheFactory.newInstance(config.cacheConfig);		
	}
	
	
	
	public MemoryCache(Properties cacheConfig) {
		config = new Config();
		cacheName = config.cacheConfig.getProperty("cacheName","WilleyCache");
		if (cacheConfig == null) 
			config.cacheConfig = config.initPropertiesDefault();
		else 
			config.cacheConfig = config.addDefaultProperties(cacheConfig);
		cache = MemoryCacheFactory.newInstance(config.cacheConfig);
	}
	
	public void backUp() {
		new Thread(new MemoryCacheBackUp(config.cacheConfig, cache)).start();
	}
	
	public void restore() {
		FutureTask<IMemoryCache>  restorer = 
			new FutureTask<IMemoryCache>
					(new MemoryCacheRestore(config.cacheConfig));
		new Thread(restorer).start();

		try {
			cache = restorer.get();
		} catch (Exception e) {
			LOG.error("FAULT  MEMORY CACHE RESTORE FROM DISK: CAUSE \n" + e.getMessage());
		}
	}
	
	public final synchronized Object put(Object key, Object value) {
		return cache.put(key, value);
	}

	public final synchronized Object putEntry(Object key, Object value) {
		return cache.putEntry(key, value);
	}

	public final synchronized Object get(Object key) {
		return cache.get(key);
	}

	public final synchronized Object getEntry(Object key) {
		return cache.getEntry(key);
	}

	public synchronized int getCapacity() {
		return cache.getCapacity();
	}
	
	public String toString() {
		StringBuffer s = new StringBuffer();
		if (cache == null) {
			LOG.error("MEMORY CACHE IS EMPTY");
			return "MEMORY CACHE IS EMPTY";
		}
		Entry e = cache.getHead();
		while (e != null) {
			s.append("" + e.getKey()).append(" = ").append("" + e.getValue()).append("   ");
			e = e.succ;
		}
		String st = s.toString(); 
		return s.toString();
	}

	public boolean isFull() {
		return cache.getCapacity() <= size();
	}

	public Object remove(Object key) {
		return cache.remove(key);
	}

	@Override
	public int size() {
		return cache.size();
	}

	@Override
	public void clear() {
		cache.clear();
	}

}
