package zrv.willeyCache.memory;

import java.io.*;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

import zrv.willeyCache.LOG;

public class testMemoryCache {
	
	private static void test(String fileName) throws InvalidPropertiesFormatException, IOException {
		MemoryCache memoryCache = new MemoryCache(fileName);
		int capacity = memoryCache.getCapacity();
		for (int i = 0; i < capacity; i++) memoryCache.put(i, i);
		System.out.println("Source memoryCache:\n  " + memoryCache);
		System.out.println(memoryCache.get(5) + ": " + memoryCache);
		System.out.println(memoryCache.get(0) + ": " + memoryCache);
		System.out.println(memoryCache.get(1) + ": " + memoryCache);
		System.out.println(memoryCache.get(9) + ": " + memoryCache);
		System.out.println(memoryCache.get(5) + ": " + memoryCache);
		System.out.println(memoryCache.get(4) + ": " + memoryCache);

		System.out.println(memoryCache.put(2, 222) + ": " + memoryCache);
		System.out.println(memoryCache.put(11, 11) + ": " + memoryCache);
		System.out.println(memoryCache.put(12, 12) + ": " + memoryCache);
		
		// ����� ���� ������ �� ���� � ��������� ������
/*		memoryCache.backUp();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		// �������������� ���� ������ � ����� � ��������� ������		
		memoryCache.restore();
		LOG.info("restored memoryCache : "
				 + "\n cacheName: " + memoryCache.config.cacheName
				 + "\n cacheConfig: " + memoryCache.config.cacheConfig
				 + "\n restored Cache : ");
		
*/
	}

	public static void main(String[] args) throws InvalidPropertiesFormatException, IOException {
		LOG.info("cache  LRU-LFU TESTING");
		test("Config_LRU-LFU.properties");
		LOG.info("cache  LRU-LFU TESTING");
		test("Config_LRU-LFU.properties");
		LOG.info("cache  LRU-LFU TESTING");
		test("Config_LFU-LFU.properties");
		LOG.info("cache  FIFO-LFU TESTING");
		test("Config_FIFO-LFU.properties");
		LOG.info("cache  RANDOM-LFU TESTING");
		test("Config_RANDOM-LFU.properties");
		LOG.info("cache  FIFO-LRU TESTING");
		test("Config_FIFO-LFU.properties");
	}

}
