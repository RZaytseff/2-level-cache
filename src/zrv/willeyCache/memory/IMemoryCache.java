package zrv.willeyCache.memory;

import java.util.Collection;
import java.util.Enumeration;
import java.util.Map;
import java.util.Set;

import zrv.willeyCache.memory.ConcurrentLinkedHashMap.Entry;

public interface IMemoryCache<K, V> {

	/**
	 * The first element that put to Cache 
	 */
	public abstract Entry getHead();

	/**
	 * The last element that put to Cache 
	 */
	public abstract Entry getTail();


	/**
	 * Returns the number of key-value mappings in this map.
	 *
	 * @return the number of key-value mappings in this map.
	 */
	public abstract int size();

	/**
	 * Returns <tt>true</tt> if this map contains no key-value mappings.
	 *
	 * @return <tt>true</tt> if this map contains no key-value mappings.
	 */
	public abstract boolean isEmpty();

	/**
	 * Returns the value to which the specified key is mapped in this table.
	 *
	 * @param   key   a key in the table.
	 * @return  the value to which the key is mapped in this table;
	 *          <code>null</code> if the key is not mapped to any value in
	 *          this table.
	 * @exception  NullPointerException  if the key is
	 *               <code>null</code>.
	 * @see     #put(Object, Object)
	 */
	public abstract V get(Object key);

	public abstract Entry getEntry(Object key);

	/**
	 * Tests if the specified object is a key in this table.
	 * 
	 * @param   key   possible key.
	 * @return  <code>true</code> if and only if the specified object 
	 *          is a key in this table, as determined by the 
	 *          <tt>equals</tt> method; <code>false</code> otherwise.
	 * @exception  NullPointerException  if the key is
	 *               <code>null</code>.
	 * @see     #contains(Object)
	 */

	public abstract boolean containsKey(Object key);

	/**
	 * Maps the specified <code>key</code> to the specified 
	 * <code>value</code> in this table. Neither the key nor the 
	 * value can be <code>null</code>. (Note that this policy is
	 * the same as for java.util.Hashtable, but unlike java.util.HashMap,
	 * which does accept nulls as valid keys and values.)<p>
	 *
	 * The value can be retrieved by calling the <code>get</code> method 
	 * with a key that is equal to the original key. 
	 *
	 * @param      key     the table key.
	 * @param      value   the value.
	 * @return     the previous value of the specified key in this table,
	 *             or <code>null</code> if it did not have one.
	 * @exception  NullPointerException  if the key or value is
	 *               <code>null</code>.
	 * @see     Object#equals(Object)
	 * @see     #get(Object)
	 */
	public abstract Object put(Object key, Object value);

	/**
	 * Removes the key (and its corresponding value) from this 
	 * table. This method does nothing if the key is not in the table.
	 *
	 * @param   key   the key that needs to be removed.
	 * @return  the value to which the key had been mapped in this table,
	 *          or <code>null</code> if the key did not have a mapping.
	 * @exception  NullPointerException  if the key is
	 *               <code>null</code>.
	 */
	public abstract V remove(Object key);

	/**
	 * Returns <tt>true</tt> if this map maps one or more keys to the
	 * specified value. Note: This method requires a full internal
	 * traversal of the hash table, and so is much slower than
	 * method <tt>containsKey</tt>.
	 *
	 * @param value value whose presence in this map is to be tested.
	 * @return <tt>true</tt> if this map maps one or more keys to the
	 * specified value.  
	 * @exception  NullPointerException  if the value is <code>null</code>.
	 */
	public abstract boolean containsValue(Object value);

	/**
	 * Tests if some key maps into the specified value in this table.
	 * This operation is more expensive than the <code>containsKey</code>
	 * method.<p>
	 *
	 * Note that this method is identical in functionality to containsValue,
	 * (which is part of the Map interface in the collections framework).
	 * 
	 * @param      value   a value to search for.
	 * @return     <code>true</code> if and only if some key maps to the
	 *             <code>value</code> argument in this table as 
	 *             determined by the <tt>equals</tt> method;
	 *             <code>false</code> otherwise.
	 * @exception  NullPointerException  if the value is <code>null</code>.
	 * @see        #containsKey(Object)
	 * @see        #containsValue(Object)
	 * @see	   Map
	 */

	public abstract boolean contains(Object value);

	/**
	 * Copies all of the mappings from the specified map to this one.
	 * 
	 * These mappings replace any mappings that this map had for any of the
	 * keys currently in the specified Map.
	 *
	 * @param t Mappings to be stored in this map.
	 */

	public abstract void putAll(Map t);

	/**
	 * Removes all mappings from this map.
	 */

	public abstract void clear();

	/**
	 * Returns a shallow copy of this 
	 * <tt>ConcurrentLinkedHashMap</tt> instance: the keys and
	 * values themselves are not cloned.
	 *
	 * @return a shallow copy of this map.
	 */

	public abstract Object clone();

	/**
	 * Returns a set view of the keys contained in this map.  The set is
	 * backed by the map, so changes to the map are reflected in the set, and
	 * vice-versa.  The set supports element removal, which removes the
	 * corresponding mapping from this map, via the <tt>Iterator.remove</tt>,
	 * <tt>Set.remove</tt>, <tt>removeAll</tt>, <tt>retainAll</tt>, and
	 * <tt>clear</tt> operations.  It does not support the <tt>add</tt> or
	 * <tt>addAll</tt> operations.
	 *
	 * @return a set view of the keys contained in this map.
	 */

	public abstract Set keySet();

	/**
	 * Returns a collection view of the values contained in this map.  The
	 * collection is backed by the map, so changes to the map are reflected in
	 * the collection, and vice-versa.  The collection supports element
	 * removal, which removes the corresponding mapping from this map, via the
	 * <tt>Iterator.remove</tt>, <tt>Collection.remove</tt>,
	 * <tt>removeAll</tt>, <tt>retainAll</tt>, and <tt>clear</tt> operations.
	 * It does not support the <tt>add</tt> or <tt>addAll</tt> operations.
	 *
	 * @return a collection view of the values contained in this map.
	 */

	public abstract Collection values();

	/**
	 * Returns a collection view of the mappings contained in this map.  Each
	 * element in the returned collection is a <tt>Map.Entry</tt>.  The
	 * collection is backed by the map, so changes to the map are reflected in
	 * the collection, and vice-versa.  The collection supports element
	 * removal, which removes the corresponding mapping from the map, via the
	 * <tt>Iterator.remove</tt>, <tt>Collection.remove</tt>,
	 * <tt>removeAll</tt>, <tt>retainAll</tt>, and <tt>clear</tt> operations.
	 * It does not support the <tt>add</tt> or <tt>addAll</tt> operations.
	 *
	 * @return a collection view of the mappings contained in this map.
	 */

	public abstract Set entrySet();

	/**
	 * Returns an enumeration of the keys in this table.
	 *
	 * @return  an enumeration of the keys in this table.
	 * @see     Enumeration
	 * @see     #elements()
	 * @see	#keySet()
	 * @see	Map
	 */
	public abstract Enumeration keys();

	/**
	 * Returns an enumeration of the values in this table.
	 * Use the Enumeration methods on the returned object to fetch the elements
	 * sequentially.
	 *
	 * @return  an enumeration of the values in this table.
	 * @see     java.util.Enumeration
	 * @see     #keys()
	 * @see	#values()
	 * @see	Map
	 */
	public abstract Enumeration elements();

	/** 
	 * increase count of Hit that usually  is read/write operation
	 * That count may be used for undertaking cache politicians 
	 * @param key
	 * @return new count
	 */
	public abstract long increaseHitCount(Object key);
	
	/**
	 * Is it occupied all places?
	 * @return Yes/No
	 */
	public boolean isFull();

	/**
	 * put Object with this key into cache 
	 * @param key of caching object
	 * @param value is a caching object
	 * @return a pair <key, object> of replaced entry with the same key or
	 * @return null if the object with key isn't exist before 
	 */
	public abstract Entry putEntry(Object key, Object value);

	/** 
	 * number of elements in Cache
	 */
	public abstract int getCapacity();
	
	public abstract String toString();

}