package zrv.willeyCache.memory;

import zrv.willeyCache.ICache;
import zrv.willeyCache.memory.ConcurrentLinkedHashMap.Entry;

public class LRUMemoryCache extends FIFOMemoryCache  implements IMemoryCache {
	
	public LRUMemoryCache (int capacity) {
		super(capacity);
	}
	
	public final synchronized Object get(Object key) {
		Entry e = getEntry(key);
		return e == null? null: e.getValue();
	}

	public final synchronized Entry getEntry(Object key) {
		Entry e = super.getEntry(key);
		
		if (e!= null) {
			// remove request entry
			e.setValue(super.remove(key));
			// Add request value to front of list
			// So, a lest recently value go to the top in list
			// and FIFOMemoryCache.put(..) remove it first.
			put(key, e.getValue());
			return e;
		}
		// key isn't exist
		return null;
	}

	@Override
	public int getCapacity() {
		return capacity;
	}
}
