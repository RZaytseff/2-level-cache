package zrv.willeyCache.memory;

import java.util.Properties;

import zrv.willeyCache.ICache;
import zrv.willeyCache.LOG;

public class MemoryCacheFactory {
	
	static IMemoryCache memoryCache = null;
	
	public static IMemoryCache newInstance() {
		return new LRUMemoryCache(1000000);
	}

	public static IMemoryCache newInstance(Properties cacheConfig) {
		if (cacheConfig == null) return newInstance();
		
		int capacity = 1000000;
		try {
			capacity = Integer.parseInt(cacheConfig.getProperty("maxElementsInMemory", "1000000"));
		} catch (Exception e) {
			LOG.info("DON'T RECOGNIZE MEMORY CACHE SIZE! DEFAULT SIZE WILL BE USED AS 1000000");
		}
		
		String policy = "LRU";  // default policy
		try {
			policy = cacheConfig.getProperty("memoryStoreEvictionPolicy", policy).trim().toUpperCase();
		} catch (Exception e) {
			LOG.info("DON'T RECOGNIZE MEMORY CACHE POLICY! DEFAULT POLICY WILL BE USED AS 'LRU'");
		}

		if (policy.equals("LRU"))    return new    LRUMemoryCache(capacity);
		if (policy.equals("LFU"))    return new    LFUMemoryCache(capacity);
		if (policy.equals("FIFO"))   return new   FIFOMemoryCache(capacity);
		if (policy.equals("RANDOM")) return new RandomMemoryCache(capacity);
		return newInstance();	
	}
}
