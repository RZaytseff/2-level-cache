package zrv.willeyCache.memory;

import java.io.*;
import java.util.*;
import java.util.concurrent.Callable;

import EDU.oswego.cs.dl.util.concurrent.ReadWriteLock;
import EDU.oswego.cs.dl.util.concurrent.WriterPreferenceReadWriteLock;

import zrv.willeyCache.ICache;
import zrv.willeyCache.LOG;
import zrv.willeyCache.disk.*;
import zrv.willeyCache.memory.IMemoryCache;
import zrv.willeyCache.memory.ConcurrentLinkedHashMap.Entry;

//TODO debug needed
public class MemoryCacheRestore implements Callable<IMemoryCache>  {
	private Properties cacheConfig;
	private IMemoryCache memoryCache;
	private IMemoryCache keyDiskIndex;
	private DiskCacheAdapter dataFile;
	private DiskCacheAdapter keyFile;
	public volatile boolean process;
	private final ReadWriteLock storageLock =  new WriterPreferenceReadWriteLock();
	
	
	public MemoryCacheRestore(Properties cacheConfig) {
		this.cacheConfig = cacheConfig;  
		this.memoryCache = MemoryCacheFactory.newInstance(cacheConfig);
		this.keyDiskIndex = MemoryCacheFactory.newInstance(cacheConfig);
		this.process = false;
	}
		
	public IMemoryCache call() {
		try {
			this.process = true;
			LOG.info("\n  Writing the memory Cash to disk is in process ... ");
				
	        String diskStoreName = cacheConfig.getProperty("memoryStoreName");
	        String diskStorePath = cacheConfig.getProperty("memoryStorePath");
	        File rafDir = new File(diskStorePath);
	        rafDir.mkdirs();
	    	
			dataFile = new DiskCacheAdapter(new File(rafDir, diskStoreName + ".data"));
			keyFile =  new DiskCacheAdapter(new File(rafDir, diskStoreName + ".key"));
		
			keyDiskIndex = loadData(keyFile, (Serializable) keyDiskIndex);
			memoryCache   = loadData(keyFile,  (Serializable) memoryCache);
			return memoryCache;
			
		} catch (Exception e) {
			LOG.error("\n  FAULT Writing the memory Cash to disk ... ");
			return null;
		} finally {
			process = false;
			LOG.info("\n  Restore memory Cache data to disk is end");

		}
				
	}
	
    private IMemoryCache loadData(DiskCacheAdapter dataFile, Serializable data) throws Exception  {
        try {
            storageLock.readLock().acquire();
            data =  dataFile.readObject(0);
            if (keyDiskIndex == null) {
            	return MemoryCacheFactory.newInstance(cacheConfig);
            }
        } catch (InterruptedException e) {
        	return null;
        } finally {
            storageLock.readLock().release();
        }
        return null;
    }

}
