package zrv.willeyCache.memory;

import java.util.Random;

import zrv.willeyCache.ICache;
import zrv.willeyCache.memory.ConcurrentLinkedHashMap.Entry;

public class FIFOMemoryCache  extends ConcurrentLinkedHashMap implements IMemoryCache {
	
	int capacity;
	
	public FIFOMemoryCache(int capacity) {
		super(capacity);
		this.capacity = capacity;
	}
	
	public final synchronized Object put(Object key, Object value) {
		Entry e = putEntry(key, value);
		return e == null? null: e.getValue();
	}

	public final synchronized Entry putEntry(Object key, Object value) {
		Entry e = getEntry(key);

		if (e	== null && isFull()) {
			// remove a first list entry
			Entry oldEntry = getHead();
			oldEntry.setValue(remove(oldEntry.getKey()));
			// Add value to front of list
			super.put(key, value);
			return oldEntry;
		}
		// Just replace the value if it exist 
		// or Add it to front of list 
		// if it isn't exist and memoryCache isn't FULL.
		return super.putHashEntry(key, value);
	}

	public boolean isFull() {
		return capacity <= size();
	}

	@Override
	public int getCapacity() {
		return capacity;
	}

	@Override
	public Entry getEntry(Object key) {
		return super.getHashEntry(key);
	}

}
