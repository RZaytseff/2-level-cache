package zrv.willeyCache.memory;

import java.io.*;
import java.util.*;

import zrv.willeyCache.ICache;
import zrv.willeyCache.LOG;
import zrv.willeyCache.disk.*;
import zrv.willeyCache.memory.IMemoryCache;
import zrv.willeyCache.memory.ConcurrentLinkedHashMap.Entry;
// TODO debug needed
public class MemoryCacheBackUp implements Runnable {
	private Properties cacheConfig;
	private IMemoryCache memoryCahe;
	private IMemoryCache keyDiskIndex;
	private DiskCacheAdapter dataFile;
	private DiskCacheAdapter keyFile;
	public volatile boolean process;
	
	public MemoryCacheBackUp(Properties cacheConfig, IMemoryCache memoryCache) {
		this.cacheConfig = cacheConfig;  
		this.memoryCahe = memoryCache;
		this.keyDiskIndex = null;
		this.process = false;
	}
		
	public void run() {
		try {
			this.process = true;
			LOG.info("\n  BackUp memory to disk in process ... ");
				
	        String memoryStoreName = cacheConfig.getProperty("memoryStoreName");
	        String memoryStorePath = cacheConfig.getProperty("memoryStorePath");
	        File rafDir = new File(memoryStorePath);
	        rafDir.mkdirs();
	    	ICache keyDiskIndex = 
				new MemoryCache
							(Integer.parseInt(cacheConfig.getProperty("maxElementsInMemory"))
											, cacheConfig.getProperty("memoryStoreEvictionPolicy"));
	    	
	    	
			DiskCacheAdapter dataFileTemp =
					new DiskCacheAdapter(new File(rafDir, memoryStoreName + "Temp.data"));
			DiskCacheAdapter keyFile =
					new DiskCacheAdapter(new File(rafDir, memoryStoreName + "Temp.key"));
		
			// write memory cache to disk Entry by Entry
			// need for remember disk index of each Entry
			Entry e = memoryCahe.getHead();
			while (e != null) {
				Serializable value = (Serializable) e.getValue();
				Serializable key = (Serializable) e.getKey();
				keyDiskIndex.put(key, dataFileTemp.appendObject(value).getPos());
				e = e.getSucc(); 
			}
			dataFileTemp.close();

			File oldData = new File(rafDir, memoryStoreName + ".data");
			if (oldData.exists())  oldData.delete();
			File newData     = new File(rafDir, memoryStoreName + "Temp.data");
			File newFileName = new File(rafDir, memoryStoreName +     ".data");
			if (newData.exists()) newData.renameTo(newFileName);
	
//			keyFile.reset();
			if (keyDiskIndex.size() > 0) 
				keyFile.writeObject((Serializable)keyDiskIndex, 0);
			
		} catch (Exception e) {
			LOG.error(" Fault BackUp memory to disk  ... \n" + e.getMessage() + e.getStackTrace());
		} finally {
			process = false;
			LOG.info("\n  BackUp memory data to disk is end");
		}
				
	}

}
