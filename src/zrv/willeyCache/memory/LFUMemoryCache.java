package zrv.willeyCache.memory;

import zrv.willeyCache.ICache;
import zrv.willeyCache.memory.ConcurrentLinkedHashMap.Entry;

public class LFUMemoryCache extends ConcurrentLinkedHashMap implements IMemoryCache {
	
	int capacity;
	
	  public LFUMemoryCache(int capacity) {
		super(capacity);
		this.capacity = capacity;
	}


	@Override
	  public Object get(Object key) {
		  Entry e = getEntry(key);
			return e==null? null:e.getValue();
		}

	@Override
	  public Entry getEntry(Object key) {
		  Entry e = super.getHashEntry(key);
		  if ( e == null) return null;
		  Object obj = e.getKey();

			if (obj	!= null) {
				// increase count access for entry
				e.increaseHitCount();
				return e;
			}
			// key isn't exist
			return null;
	}

	  	
	private final synchronized Entry removeLFUentry() {
		Entry lowestElement, e = getHead();
		lowestElement = e; 
		while (e != null) {
			if (e.getHitCount() < lowestElement.getHitCount()) 
				lowestElement = e;
			e = e.getSucc();
		}
		lowestElement.setValue(remove(lowestElement.getKey()));
		return lowestElement;
		
	}

	public final synchronized Object put(Object key, Object value) {
		Entry e = putEntry(key, value);
		return e == null? null: e.getValue();
	}

	public final synchronized Entry putEntry(Object key, Object value) {
		Object obj = get(key);
		if (obj	== null && isFull()) {
			// remove a least frequency used entry
			Entry oldEntry = removeLFUentry();
			// Add value to front of list
			super.putHashEntry(key, value);
			return oldEntry;
		}
		// Just replace the value if it exist 
		// or Add it to front of list if it isn't exist and memoryCache isn't FULL.
		return super.putHashEntry(key, value);
	}

	public boolean isFull() {
		return capacity <= size();
	}


	@Override
	public int getCapacity() {
		return capacity;
	}
}
