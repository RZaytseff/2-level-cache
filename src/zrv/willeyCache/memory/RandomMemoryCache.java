package zrv.willeyCache.memory;

import java.util.Random;

import zrv.willeyCache.ICache;
import zrv.willeyCache.memory.ConcurrentLinkedHashMap.Entry;

public class RandomMemoryCache  extends ConcurrentLinkedHashMap implements IMemoryCache {
	
	int capacity;
	
	public RandomMemoryCache(int capacity) {
		super(capacity);
		this.capacity = capacity;
	}
	
	public final synchronized Object put(Object key, Object value) {
		Entry e = putEntry(key, value);
		return e == null? null: e.getValue();
	}

	public final synchronized Entry putEntry(Object key, Object value) {
		Object obj = get(key);

		if (obj	== null && isFull()) {
			// Random index of List
			int index = (int) (size() * new Random().nextFloat());
			
			Entry oldEntry = getListEntry(index);
			// remove a Random list entry
			oldEntry.setValue(remove(oldEntry.getKey()));
			// Add value to front of list
			super.put(key, value);
			return oldEntry;
		}
		// Just replace the value if it exist 
		// or Add it to front of list if it isn't exist and memoryCache isn't FULL.
		return super.putHashEntry(key, value);
	}

	private Entry getListEntry(int index) {
		if (index < 0 || index > size()) return null;
		Entry e = null;
		if (index < size() /2) {
			e  = getHead();
			for (int i = 1; i < index; i++) {
				e = e.getSucc();
			if (e == null) return null;
			}
		} else {
			e  = getTail();
			for (int i = index; i > 1; i--) {
				e = e.getPred();
				if (e == null) return null;
			}		
		}
		return e;		
	}

	public boolean isFull() {
		return capacity <= size();
	}

	@Override
	public int getCapacity() {
		return capacity;
	}

	@Override
	public Entry getEntry(Object key) {
		return super.getHashEntry(key);
	}
}
