package zrv.willeyCache;

public interface ICache{
	/**
	 *  @return old replacing value or null if it is new
	 * @param key
	 * @param value
	 * @return
	 */
	public Object put(Object key,Object value);
	/**
	 *  @return request value or null if it isn't exist
	 * @param key
	 * @return
	 */
	public Object get(Object key);
	
	public Object remove(Object key);
	
	public void clear();
	
	/**
	 * BackUp Cache to disk
	 */
	public abstract void backUp();

	/**
	 * Restore Cache from disk
	 */
	public abstract void restore();

	/**
	 * real size of Cache
	 * @return
	 */
	public int size();
	
	public int getCapacity();
	
	public String toString();
}
