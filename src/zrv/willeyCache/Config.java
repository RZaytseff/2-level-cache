package zrv.willeyCache;

import java.io.FileInputStream;
import java.util.Properties;

public class Config {

	public String cacheName;
	
    /** the filename of config properties*/
	public String configFileName;

    /** the filename of default config properties*/
	public String configFileNameDefault;

	public Properties cacheConfig;
	public Properties cacheConfigDefault;
	private Properties configLoad;
	
	public final static String[][] configDefault = 
								{	{"cacheName", "WilleyCache"},
									{"overflowToDisk", "true"},
		
									{"memoryStoreEvictionPolicy", "LRU"},
									{"memoryBackUpThreadIntervalSeconds", "360"},
									{"maxElementsInMemory", "1000000"},
									{"memoryStorePath", "C:/TMP"},
									{"memoryStoreName", "WilleyMemoryStore"},
									
									{"diskStoreEvictionPolicy", "LFU"},
									{"maxElementsOnDisk", "1000000"},
									{"diskStoreName", "willeyDiskStore"},
									{"diskStorePath", "C:/TMP"},
									{"diskBackUpThreadIntervalSeconds","3600"}
								};

	
	public Config() {
		init();
	}
	
	public Config(String configFile) {
		init(configFile);
	}
	
	// need for hot RE INIT  CACHE CONFIG
	public Properties init() {
		if (configFileName == null) configFileName = "Config.properties";
		return init(configFileName);
	}
	
	public Properties init(Properties cacheConfig) {
		if (cacheConfig == null) cacheConfig = initPropertiesDefault();
		return this.cacheConfig = addDefaultProperties(cacheConfig);
	}
	
	public synchronized Properties init(String configFile) {
		if (cacheConfigDefault == null) initPropertiesDefault();
		configFileName = configFile;
		
		if (configFile == null) configFileName = "Config.properties";
		else configFileName = configFile;
		
	   	configLoad = new Properties();
	   	try {
		   	FileInputStream in = new FileInputStream(configFileName);
		   	configLoad.load(in);
	   	} catch (Exception e) {
	   		LOG.error("�� ������ ���� ������������ " + configFileName);
			return cacheConfigDefault;
		}
	   	return cacheConfig = addDefaultProperties(configLoad);
	   	
	}

	public Properties addDefaultProperties(Properties cacheConfig) {
		if (cacheConfigDefault == null) initPropertiesDefault();
		for (int i = 0; i < configDefault.length; i++) 
			cacheConfig.setProperty(configDefault[i][0], 
					cacheConfig.getProperty(configDefault[i][0], configDefault[i][1]));
		return 	cacheConfig;
		}
			
	public synchronized Properties initPropertiesDefault() {
		cacheName = "WilleyCache";
    	configFileName = "Config.properties";
    	configFileNameDefault = "ConfigDefault.properties";
    	return initPropertiesDefault(configFileNameDefault); 
	}
	// need for hot RE INIT  CACHE CONFIG Default
	public synchronized Properties initPropertiesDefault(String configFileNameDefault) {
		if (configFileNameDefault == null) configFileNameDefault = "ConfigDefault.properties";
		this.configFileNameDefault = configFileNameDefault;
    	Properties configLoad = new Properties();
	   	try {
		   	FileInputStream in = new FileInputStream(configFileNameDefault);
		   	configLoad.load(in);
		   	return cacheConfigDefault = configLoad;
		   	
	   	} catch (Exception e) {
	   		LOG.info("�� ������ ���� ������������ �� ��������� ConfigDefault.properties");
	   		LOG.info("�������� ��������� ������������ ����� ��������� �� ���������!");
	   		cacheConfigDefault = new Properties();
	   		for (int i = 0; i < configDefault.length; i++) 
	   			cacheConfigDefault.setProperty(configDefault[i][0], configDefault[i][1]);
	   		return cacheConfigDefault;
		}
	}
	
	public String getProperty(String propertyName) {
		return cacheConfig.getProperty(propertyName);
	}
	
	public String getProperty(String propertyName, String defaultProprtyValue) {
		return cacheConfig.getProperty(propertyName, defaultProprtyValue);
	}
	
	public String setProperty(String key, String value) {
		return cacheConfig.getProperty(key, value);
	}
	
	public String setPropertyDefault(String key, String value) {
		return cacheConfigDefault.getProperty(key, value);
	}
}
