package zrv.willeyCache;

import java.io.*;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

import zrv.willeyCache.memory.IMemoryCache;
import zrv.willeyCache.memory.MemoryCache;
import zrv.willeyCache.memory.MemoryCacheRestore;

public class testCache  {
	
	static Cache cache;
	
	interface  Tests {
		int Test_1 = 1; 
		int Test_2 = 2;
		int Test_3 = 3;
	}
	
	enum Test {Test_1, Test_2, Test_3}
	
	private static void testStrategy(String fileName) throws InvalidPropertiesFormatException, IOException {
		// create Cache with periodical disk optimization
		cache = new Cache(fileName, true); 
		for (int i = 0; i < cache.getCapacity(); i++) cache.put(i, i);
		System.out.println("Source cache:\n  " + cache);
		LOG.info("�������� �������� " + cache.get(5));
		System.out.println(cache);
		LOG.info("�������� �������� " + cache.get(0));
		System.out.println(cache);
		LOG.info("�������� �������� " + cache.get(1));
		System.out.println(cache);
		LOG.info("�������� �������� " + cache.get(9));
		System.out.println(cache);
		LOG.info("�������� �������� " + cache.get(5));
		System.out.println(cache);
		LOG.info("�������� �������� " + cache.get(4));
		System.out.println(cache);

		LOG.info("�������� �������� " + cache.put(22, 22));
		System.out.println(cache);
		LOG.info("�������� �������� " + cache.put(21, 21));
		System.out.println(cache);
		LOG.info("�������� �������� " + cache.put(1, 1));
		System.out.println(cache);
		LOG.info("�������� �������� " + cache.put(2, 22));
		System.out.println(cache);
		LOG.info("�������� �������� " + cache.put(14, 14));
		System.out.println(cache);
		LOG.info("�������� �������� " + cache.put(15, 15));
		System.out.println(cache);
		
		LOG.info("�������� �������� " + cache.get(2));
		System.out.println(cache);
		LOG.info("�������� �������� " + cache.get(11));
		System.out.println(cache);
		LOG.info("�������� �������� " + cache.get(12));
		System.out.println(cache);
		
		LOG.info("�������� �������� " + cache.put(2, 222));
		System.out.println(cache);
		LOG.info("�������� �������� " + cache.put(11, 11));
		System.out.println(cache);
		LOG.info("�������� �������� " + cache.put(12, 12));
		System.out.println(cache);
		LOG.info("�������� �������� " + cache.put(2, 222));
		System.out.println(cache);
		LOG.info("�������� �������� " + cache.put(11, 11));
		System.out.println(cache);
		LOG.info("�������� �������� " + cache.put(12, 12));
		System.out.println(cache);		

		LOG.info("�������� �������� " + cache.put(32, 32));
		System.out.println(cache);
		LOG.info("�������� �������� " + cache.put(31, 31));
		System.out.println(cache);
		LOG.info("�������� �������� " + cache.put(1, 1));
		System.out.println(cache);
		LOG.info("�������� �������� " + cache.put(2, 22));
		System.out.println(cache);
		LOG.info("�������� �������� " + cache.put(24, 24));
		System.out.println(cache);
		LOG.info("�������� �������� " + cache.put(25, 25));
		System.out.println(cache);
		
		LOG.info("�������� �������� " + cache.get(2));
		System.out.println(cache);
		LOG.info("�������� �������� " + cache.get(20));
		System.out.println(cache);
		LOG.info("�������� �������� " + cache.get(12));
		System.out.println(cache);
		
		boolean eternal =  Boolean.parseBoolean(cache.cacheConfig.getProperty("eternal",  "false"));
		//  TODO debug is needed
		if (eternal) {
		// ����� ���� ������ �� ���� � ��������� ������
		cache.backUp();
		try {
			Thread.sleep(12000);
		} catch (InterruptedException e) {
			LOG.error("TREADS MONITOR ERROR!!!" + e.getMessage());
			e.printStackTrace();
		}
		// �������������� ���� ������ � ���� � ��������� ������		
		cache.restore();
		System.out.println(cache);
		}
	}

	private static double[] testPerformance(String fileName, boolean optimization) 
								throws InvalidPropertiesFormatException, IOException {
		// create Cache with or no periodical disk optimization
		cache = new Cache(fileName, optimization);
		double[] ret = {0, 0};
		try {
			ret = new Performancer(cache).call();
		} catch (Exception e) {
			LOG.error("testPerformance DON'T  executed");
			e.printStackTrace();
		}
		return ret;
	}

		
	private static double[] testConcurrentPerformance(int threadsNumber) {
		double get, put, res[] = new double[2];
		get = put = 0;
		
		Vector  perfornamcsVector = new Vector();
		FutureTask<double[]>  performancer = 
			new FutureTask<double[]>
					(new Performancer(cache));
		for (int i = 0; i < threadsNumber; i ++) {
			performancer = 
				new FutureTask<double[]>
						(new Performancer(cache));
			perfornamcsVector.add(performancer);
				new Thread(performancer).start();
		}
		for (int i = 0; i < threadsNumber; i ++) {
			performancer = null;
			try {
				performancer =  (FutureTask<double[]>) perfornamcsVector.get(i);
				res =((FutureTask<double[]>) performancer).get();
			} catch (Exception e) {
				LOG.error("FAULT  PERFORMANCE TREAD #"+ i + " perfornamcer = " + performancer 
				                         + ":\n CAUSE \n" + e.getMessage());
			}
			get+=res[0];
			put+=res[1];
		}
		
		res[0] = get/=threadsNumber;
		res[1] = put/=threadsNumber;
		return res;

	}
	
	public static void main(String[] args) throws InvalidPropertiesFormatException, IOException {
//		int testNumber = 3;
		for (Test testNumber: Test.values())
		switch (testNumber) {
			case
			Test_1: {
					System.out.println("\n\n*******************************************************************************************************");
					System.out.println("\n\n    TEST VARIOUS CACHING STARTEGY");		
					System.out.println("\n\n*******************************************************************************************************");
			
					System.out.println("\n\n---------------------------------------------------------------");
					System.out.println("\n  TEST LRU-LFU cache \n ");
					testStrategy("Config_LRU-LFU.properties");
					System.out.println("\n\n---------------------------------------------------------------");
					System.out.println("\n TEST LRU-NULL cache WITHOUT DISK CACHING\n ");
					testStrategy("Config_LRU-NULL.properties");
					System.out.println("\n\n---------------------------------------------------------------");
					System.out.println("\n TEST  LRU-LRU  cache \n ");
					testStrategy("Config_LRU-LRU.properties");
					System.out.println("\n\n---------------------------------------------------------------");
					System.out.println("\n TEST  LFU-LFU  cache \n ");
					testStrategy("Config_LFU-LFU.properties");
					System.out.println("\n\n---------------------------------------------------------------");
					System.out.println("\n TEST  FIFO-LFU  cache \n ");
					testStrategy("Config_FIFO-LFU.properties");
					System.out.println("\n\n---------------------------------------------------------------");
					System.out.println("\n TEST  RANDOM-LFU  cache \n ");
					testStrategy("Config_RANDOM-LFU.properties");
					System.out.println("\n\n---------------------------------------------------------------");
					System.out.println("\n TEST  FIFO-LRU  cache \n ");
					testStrategy("Config_RANDOM-LFU.properties");
					
				}
			case 
			Test_2: {
					System.out.println("\n\n*******************************************************************************************************");
					System.out.println("\n\n                                TEST PERFORMANCE");
					System.out.println("\n ������������� ������� �������� �������� ������/������ ����"); 
					System.out.println("\n ����������� 10 ����� �������, � ������ ����� �� 10 - 5000 ��������"); 					System.out.println("\n\n*******************************************************************************************************");
					System.out.println("\n\n---------------------------------------------------------------");
					System.out.println("\n TEST  PERFORMANCE LRU-LFU  Without Disk CACHE \n ");
					System.out.println("\n\n---------------------------------------------------------------");
					int n = 10;   // number of batch of series 
					double get, put, res[] = new double[2];
					get = put = 0;
					for (int i = 0; i < n; i ++) {
						res = testPerformance("Config_LRU-LFU-PerformanceWithoutDiskCACHE.properties", false);
						get+=res[0];
						put+=res[1];
					}
					get/=n;
					put/=n;
					System.out.println("\n----------------------------------------------------------------------");
					System.out.println("\n AVERAGE: \n");
					System.out.print  ("Get average for MyFramework = " + get + "     ");
					System.out.println("Put average for MyFramework = " + put);
					System.out.println("\n----------------------------------------------------------------------");
// ------------------------------------------			
					System.out.println("\n\n---------------------------------------------------------------");
					System.out.println("\n TEST  PERFORMANCE LRU-LFU  Whith Disk CACHE \n ");
					System.out.println("\n\n*******************************************************************************************************");
					System.out.println("\n\n---------------------------------------------------------------");
			
					get = put = 0;
					for (int i = 0; i < n; i ++) {
						res = testPerformance("Config_LRU-LFU-PerformanceWithDiskCACHE.properties", false);
						get+=res[0];
						put+=res[1];
					}
					get/=n;
					put/=n;
					System.out.println("\n----------------------------------------------------------------------");
					System.out.println("\n AVERAGE: \n");
					System.out.print  ("Get average for MyFramework = " + get + "     ");
					System.out.println("Put average for MyFramework = " + put);
					System.out.println("\n----------------------------------------------------------------------");
				}
			
			case 
			Test_3: {
					System.out.println("\n\n*******************************************************************************************************");
					System.out.println("\n\n    TEST CONCURRENT PERFORMANCE");		
					System.out.println("\n ������������ �� 25 ������� ������������ ����� �������� ������/������ ����");
					System.out.println("\n ����� ������������� ������� ����� ���������� ��������"); 
					System.out.println("\n\n ������� ���� - ���������, ���:");
					System.out.println("\n     -  �� ��������� �� ������ ���� � �� ���� ����� �� ������� �������");
					System.out.println("\n     -  ������������� �������� �� ����������� �� ��������� � ������ �2 - ��� ������������ �������");
					System.out.println("\n\n*******************************************************************************************************");

					int threadsNumber = 25;
					double[] res;
					System.out.println("\n\n**************************    number of Treads is " + threadsNumber + "        *******************************************");
					
					
					System.out.println("\n\n---------------------------------------------------------------");
					System.out.println("\n TEST  CONCURRENT PERFORMANCE LFU-LFU  Without Disk CACHE \n ");
					
					cache = new Cache("Config_LRU-LFU-PerformanceWithoutDiskCACHE.properties", false);
					res = testConcurrentPerformance(threadsNumber);
					System.out.println("\n----------------------------------------------------------------------");
					System.out.println("\n AVERAGE: \n");
					System.out.print  ("Get average for MyFramework = " + res[0] + "     ");
					System.out.println("Put average for MyFramework = " + res[1]);
					System.out.println("\n----------------------------------------------------------------------");
			//-------------------------
					
					System.out.println("\n\n---------------------------------------------------------------");
					System.out.println("\n TEST  PERFORMANCE LRU-LFU  With Disk CACHE \n ");
					
					cache = new Cache("Config_LRU-LFU-PerformanceWithDiskCACHE.properties", false);
					res = testConcurrentPerformance(threadsNumber);
					System.out.println("\n----------------------------------------------------------------------");
					System.out.println("\n AVERAGE: \n");
					System.out.print  ("Get average for MyFramework = " + res[0] + "     ");
					System.out.println("Put average for MyFramework = " + res[1]);
					System.out.println("\n----------------------------------------------------------------------");
				}
			}
// after all test need stop disk optimizer		
		try {
			Thread.sleep(60*1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		// stop
		cache.diskOptimizerGO(false);
	}

}
